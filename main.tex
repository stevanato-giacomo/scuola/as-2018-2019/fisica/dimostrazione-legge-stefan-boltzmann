\documentclass[a4paper, 12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{hyperref}

\hypersetup{
    colorlinks=true,
    linkcolor=blue,  
    urlcolor=blue,
}
\allowdisplaybreaks
\newcommand\ddfrac[2]{\frac{\displaystyle #1}{\displaystyle #2}}

\title{
    Dimostrazione della \\
    Legge di Stefan-Boltzmann
}
\author{Stevanato Giacomo}
\date{16 Aprile 2019}

\begin{document}
    \maketitle

    \paragraph*{Funzione di partenza}

    \begin{equation*}
        B_\nu \left(\nu, T\right) = \frac{2 h \nu^3}{c^2}\frac{1}{e^{\frac{\nu h}{k_B T}}-1}
    \end{equation*}

    \paragraph*{Semplificazione dell'integrale}

    \begin{align*}
        I(T) &= \int cos\left(\theta\right) d\Omega \int_{0}^{+\infty} B_\nu \left(\nu, T\right) d\nu = \\
        &= \int cos\left(\theta\right) sin\left(\theta\right) d\theta d\phi \int_{0}^{+\infty} B_\nu \left(\nu, T\right) d\nu = \\
        &= \int_{0}^{2 \pi} d\phi \int_{0}^{\frac{\pi}{2}} sin\left(\theta\right)cos\left(\theta\right) d\theta \int_{0}^{+\infty} B_\nu \left(\nu, T\right) d\nu = \\
        &= \left[\phi\right]_{0}^{2 \pi} \left[-\frac{cos\left(2 \theta\right)}{4}\right]_{0}^{\frac{\pi}{2}} \int_{0}^{+\infty} B_\nu \left(\nu, T\right) d\nu = \\
        &= \pi \int_{0}^{+\infty} B_\nu \left(\nu, T\right) d\nu = \\
        &= \int_{0}^{+\infty} \frac{2 \pi h \nu^3}{c^2}\frac{1}{e^{\frac{\nu h}{k_B T}}-1} d\nu \\
        &\text{
            Pongo $\lambda = \frac{c}{\nu}$, quindi $\nu = \frac{\lambda}{c}$ e $d\nu = -\frac{c}{\lambda^2}d\lambda$
        } \\
        &= \int_{+\infty}^{0} -\frac{c}{\lambda^2} \frac{2 \pi h c}{\lambda^3}\frac{1}{e^{\frac{h c}{k_B T \lambda}}-1} d\lambda = \\
        &= \int_{0}^{+\infty} \frac{2 \pi h c^2}{\lambda^5} \frac{1}{e^{\frac{h c}{k_B T \lambda}}-1} d\lambda = \\
        &\text{
            Pongo $x = \frac{h c}{k_B T \lambda}$, quindi $\lambda = \frac{h c}{k_B T x}$ e $d\lambda = -\frac{h c}{k_B T x^2}$ 
        } \\
        &= \int_{+\infty}^{0} 2 \pi h c^2 \frac{k_B^5 T^5 x^5}{h^5 c^5} \frac{1}{e^x - 1} \frac{- h c}{k_B T x^2} dx = \\
        &= \int_{0}^{+\infty} \frac{2 \pi k_B^4 T^4 x^3}{h^3 c^2} \frac{1}{e^x - 1} dx = \\
        &= \frac{2 \pi k_B^4 T^4}{h^3 c^2} \int_{0}^{+\infty} \frac{x^3}{e^x - 1} dx = 
    \end{align*}

    \paragraph*{Calcolo dell'integrale semplificato}

    \begin{align*}
        &\int_{0}^{+\infty} \frac{x^3}{e^x - 1} dx = \int_{0}^{+\infty} \frac{x^3 e^{-x}}{1-e^{-x}} dx = \\
        &\text{
            Sapendo che $\frac{1}{1-n} = \sum_{i=0}^{+\infty} n^i$ se $|n| < 1$ e $|e^{-x}| < 1 \forall x > 0$
        } \\
        &\text{
            Nota: Lo $0$ è già escluso dal dominio
        } \\
        &= \int_{0}^{+\infty} \sum_{i=0}^{+\infty} x^3 e^{-x \left( i+1 \right)} dx = \\
        &\text{
            Poiche $x^3 e^{-x \left( i+1 \right)}$ è continua e positiva $\forall x > 0$ allora:
        } \\
        &= \sum_{i=0}^{+\infty} \int_{0}^{+\infty} x^3 e^{-x \left( i+1 \right)} dx =
    \end{align*}

    \begin{center}
        \def\arraystretch{2}\tabcolsep=10pt
        \begin{tabular}{ c c c }
            \toprule
                & D      & I   \\
            \midrule
            $+$ & $x^3$  & $e^{-x \left(i+1\right)}$ \\
            $-$ & $3x^2$ & $\ddfrac{-e^{-x \left(i+1\right)}}{\left(i+1\right)}$   \\
            $+$ & $6x$   & $\ddfrac{e^{-x \left(i+1\right)}}{\left(i+1\right)^2}$  \\
            $-$ & $6$   & $\ddfrac{-e^{-x \left(i+1\right)}}{\left(i+1\right)^3}$ \\
            $+$ & $0$    & $\ddfrac{e^{-x \left(i+1\right)}}{\left(i+1\right)^4}$  \\
            \bottomrule
        \end{tabular}
    \end{center}

    Poiché ogni $k x^n e^{-x\left(i+1\right)}$ per $n > 0$ tende a $0$ sia se $x$ tende $0$ sia se tende a $+\infty$, l'unico addendo non nullo è $\ddfrac{6 e^{-x \left(i+1\right)}}{\left(i+1\right)^4}$

    \begin{equation*}
        = \sum_{i=0}^{+\infty} \left[\ddfrac{6 e^{-x \left(i+1\right)}}{\left(i+1\right)^4}\right]_{0}^{+\infty} = 6 \sum_{i=0}^{+\infty} \ddfrac{1}{(i+1)^4} = 6 \sum_{i=1}^{+\infty} \ddfrac{1}{i^4}
    \end{equation*}

    Ricapitolando:

    \begin{equation*}
        I(T) = \frac{2 \pi k_B^4 T^4}{h^3 c^2} \int_{0}^{+\infty} \frac{x^3}{e^x - 1} dx = \frac{12 \pi k_B^4 T^4}{h^3 c^2} \sum_{i=1}^{+\infty} \frac{1}{i^4}
    \end{equation*}

    \paragraph*{Calcolo della serie}

    Consideriamo la serie di Fourier di $f\left(x\right) = x^2$ con $-\pi < x < \pi$.

    \begin{equation*}
        c_0 = \frac{1}{2 \pi} \int_{-\pi}^{\pi} x^2 dx = \frac{1}{2 \pi} \left[\frac{x^3}{3}\right]_{-\pi}^{\pi} = \frac{\pi^2}{3}
    \end{equation*}

    \begin{align*}
        c_n &= \frac{1}{2 \pi} \int_{-\pi}^{\pi} x^2 e^{i n x} dx = \frac{1}{2 \pi} \int_{-\pi}^{\pi} x^2 \left[cos\left(n x\right) + i sin\left(n x\right)\right] dx =\\
        &= \frac{1}{2 \pi} \int_{-\pi}^{\pi} x^2 cos\left(n x\right) dx = \frac{1}{\pi} \int_{0}^{\pi} x^2 cos\left(n x\right) =
    \end{align*}

    \begin{center}
        \def\arraystretch{2}\tabcolsep=10pt
        \begin{tabular}{ c c c }
            \toprule
                & D      & I   \\
            \midrule
            $+$ & $x^2$ & $cos\left(n x\right)$ \\
            $-$ & $2x$  & $\ddfrac{sin\left(n x\right)}{n}$ \\
            $+$ & $2$   & $-\ddfrac{cos\left(n x\right)}{n^2}$ \\
            $-$ & $0$   & $-\ddfrac{sin\left(n x\right)}{n^3}$  \\
            \bottomrule
        \end{tabular}
    \end{center}

    Ogni termine col seno si annullerà visto che $sin\left(0\right) = 0$ e $sin\left(n\pi\right) = 0$. Rimaniamo quindi con $\ddfrac{2 x \cdot cos\left(n x\right)}{n^2}$ \\
    Ricordiamo inoltre che $cos\left(n \pi \right) = \left(-1\right)^n$

    \begin{equation*}
        = \frac{1}{\pi} \left[\frac{2 x \cdot cos\left(n x\right)}{n^2}\right]_{0}^{\pi} = \frac{2 \left(-1\right)^n}{n^2}
    \end{equation*}

    Usando l'identità di Parseval

    \begin{equation*}
        \sum_{n=-\infty}^{+\infty} c_n^2 = \frac{1}{2\pi}\int_{-\pi}^\pi x^4 dx = \frac{1}{2 \pi} \left[\frac{x^5}{5}\right]_{-\pi}^{\pi} = \frac{\pi^4}{5}
    \end{equation*}
    \begin{equation*}
        \sum_{n=-\infty}^{+\infty} c_n^2 = c_0^2 + 2 \sum_{n=1}^{+\infty}c_n^2 = \frac{\pi^4}{9} + 8 \sum_{n=1}^{+\infty} \frac{1}{n^4}
    \end{equation*}

    \begin{equation*}
        \frac{\pi^4}{9} + 8 \sum_{n=1}^{+\infty} \frac{1}{n^4} = \frac{\pi^4}{5}
    \end{equation*}
    \begin{equation*}
        \sum_{n=1}^{+\infty} \frac{1}{n^4} = \frac{1}{8} \left( \frac{\pi^4}{5} - \frac{\pi^4}{9} \right) = \frac{\pi^4}{90}
    \end{equation*}

    Ricapitolando (di nuovo):

    \begin{equation*}
        I(T) = \frac{12 \pi k_B^4 T^4}{h^3 c^2} \sum_{i=1}^{+\infty} \frac{1}{i^4} = \frac{12 k_B^4 T^4 \pi^4}{90 h^3 c^2} = \frac{2 \pi^5 k_B^4}{15 h^3 c^2} T^4 = \sigma T^4
    \end{equation*}
    \begin{equation*}
        \text{Con } \sigma = \frac{2 \pi^5 k_B^4}{15 h^3 c^2}
    \end{equation*}

    \section*{Fonti}
    \begin{enumerate}
        \item \href{https://it.wikipedia.org/wiki/Corpo_nero}{Corpo nero - Wikipedia}
        \item \href{https://it.wikipedia.org/wiki/Legge_di_Stefan-Boltzmann}{Legge di Stefan-Boltzmann - Wikipedia}
        \item \href{https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_law}{Stefan-Boltzmann's Law - Wikipedia}
        \item \href{https://it.wikipedia.org/wiki/Serie_di_Fourier}{Serie di Fourier - Wikipedia}
        \item \href{https://it.wikipedia.org/wiki/Identit%C3%A0_di_Parseval}{Identità di Parseval - Wikipedia}
        \item \href{https://math.stackexchange.com/questions/99843/contour-integral-for-x3-ex-1}{complex analysis - Contour integral for $\frac{x^3}{e^x-1}$? - Mathematics Stack Exchange}
        \item \href{https://math.stackexchange.com/questions/650966/evaluate-sum-infty-n-1-frac1n4-using-parsevals-theorem-fourier-ser}{calculus - Evaluate $\sum^\infty_{n=1} \frac{1}{n^4}$ using Parseval's theorem (Fourier series) - Mathematics Stack Exchange}
    \end{enumerate}
    
\end{document}